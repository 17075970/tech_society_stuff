from pygame_tools.Geometry import Spline
from pygame_tools.Geometry.Transform import Transform


m = []
index = 0

cones = []

def add_segment(start: Transform, finish: Transform):
    global m

    m += Spline.get(start, finish, tc=100)


def add_cone(x: float, y: float):
    global cones
    
    cone = Transform(x, y)
    cones.append(cone)

def get_path():
    global m
    return m


def get_next_point() -> Transform:
    global m
    global index

    point = m[index]

    index += 1
    index %= len(m)

    return point

def get_cones() -> list:
    global cones

    return cones
