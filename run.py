from pygame_tools import *
from pygame_tools.Geometry import Spline
from pygame_tools.Timer import Timer
import sys

import time
from math import pi, cos, sin

import Map
import todo_calculation_file as calc
import random


if __name__ == "__main__":
    pg.init()
    set_win_size(800, 800)
    flip_along_y(True)

    calc.init()

    noisy_cones = []
    t = Timer()

    while True:
        cones = Map.get_cones()
        path = Map.get_path()
        rel_cones = []

        car_pos = Map.get_next_point()

        

        for cone in cones:
            rel_cone = car_pos.get_offset(cone)


            if t.time_from_start() > 0.2:
                if rel_cone.get_x() > 0 and rel_cone.get_mag() < 100:
                    noise = 25 - random.random() * 50, 25 - random.random() * 50
                    rel_cones.append(rel_cone.with_offset(*noise))

                    noisy_cones.append(calc.calculate_cone_real_pose(car_pos, rel_cones[-1]))
                t.restart()

        calc.loop(car_pos, rel_cones)
        
        for event in pg.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        background(BLACK)

        color(255, 255, 0)
        for n in range(len(noisy_cones)):
            ellipse(*noisy_cones[n].to_xy(), 5, 5)

        color(RED)
        for n in range(len(cones)):
            ellipse(*cones[n].to_xy(), 10, 10)

        color(GREY)
        for n in range(1, len(path)):
            x1, y1 = path[n - 1].to_xy()
            x2, y2 = path[n].to_xy()

            line(x1, y1, x2, y2)

        x1, y1 = path[-1].to_xy()
        x2, y2 = path[0].to_xy()

        line(x1, y1, x2, y2)

        color(WHITE)
        x, y, a = car_pos.to_xya()
        ellipse(x, y, 10, 10)
        ellipse(x + cos(a) * 15, y + sin(a) * 15, 5, 5)


        '''color(RED)
        ellipse(*start.to_xy(), 15, 15)
        ellipse(*finish.to_xy(), 15, 15)'''

        update_display()

        time.sleep(0.01)
