import Map
from pygame_tools.Geometry.Transform import Transform
from math import pi


def init():
    start = Transform(300, 220, pi)
    finish = Transform(300, 520, 0)

    Map.add_segment(start, finish) 

    start = Transform(300, 520, 0)
    finish = Transform(240, 330, -pi/2   )
    
    Map.add_segment(start, finish)

    start = Transform(240, 330, -pi/2   )
    finish = Transform(300, 220, pi)
    
    Map.add_segment(start, finish)

    Map.add_cone(240, 420)

def calculate_cone_real_pose(car_pos: Transform, rel_cone_pos: Transform):
    return car_pos.with_offset_t(rel_cone_pos)

def loop(car_pos: Transform, rel_cones: list):
    pass
